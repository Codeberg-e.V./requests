---
name: "Increase repository limit"
about: "Tell us if you need more than 100 repositories"
title: "[REPOS] "
labels: resources/limit
---

Hi, I would like my limit for repositories to be lifted.

Please increase my limit to ___ repositories.

- [x] You can already find my work on my Codeberg profile, I just need some more ...
- [ ] I want to move content from somewhere else (please link): 


- [ ] My global resource usage will remain below 750 MiB with all the repositories
- [ ] I also want to request more storage for my projects

Further comments:
